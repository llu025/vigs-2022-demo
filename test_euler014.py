import euler014 as E

def test_collatz_len_returns_tuple():
    assert len(E.collatz_len(1)) == 2


def test_collatz_step_odd():
    assert E.collatz_step(3) == 10


def test_collatz_step_even():
    assert E.collatz_step(10) == 5

def test_collatz_sequence_expected_length():
    assert E.collatz(13) == 10

def test_collatz_longest_candidate_identified():
    # Between 1 and 5, the longest sequence is
    # 3 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
    # with 8 elements
    assert E.collatz_len(6) == (3, 8)