import sys

def collatz_step(i):
    return i/2 if i % 2 == 0 else (3*i + 1)

def collatz(i):
    current, steps = i, 1
    while current != 1:
        current = collatz_step(current)
        steps += 1
    return steps

def collatz_len(N):
    best_candidate, best_length = 0, 0
    for candidate in range(1, N):
        length = collatz(candidate)
        if length > best_length:
            best_candidate, best_length = candidate, length

    return best_candidate, best_length

if __name__ == "__main__":
    # should really use argparse here
    if len(sys.argv) < 2:
        print(f"Usage: {__file__} NNN")
        exit(1)

    max_N = int(sys.argv[1])
    start, length = collatz_len(max_N)
    print(f"The longest collatz chain up to {max_N} starts at {start} and has {length} elements.")
